import { checkSchema } from 'express-validator';

const loginSchema = {
  username: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`username` cannot be empty',
      },
    },
    matches: {
      options: /^[a-zA-Z0-9._]*$/,
      errorMessage: {
        type: 'param_invalid',
        message: '`username` is invalid',
      },
    },
  },
  password: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`password` cannot be empty',
      },
    },
    matches: {
      options: /^[a-zA-Z0-9._]*$/,
      errorMessage: {
        type: 'param_invalid',
        message: '`password` is invalid',
      },
    },
  },
};

const validate = (method) => {
  switch (method) {
    case 'login': {
      return [checkSchema(loginSchema)];
    }
  }
};

export default validate;
