import * as express from 'express';
import controller from './controller';
import validate from './validate';

export default express
  .Router()
  .post('/login', validate('login'), controller.login);
