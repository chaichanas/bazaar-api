import { StatusCodes } from 'http-status-codes';
import validateResult from '../../utils/validateResult';
const { QueryTypes } = require('sequelize');
const logger = require('../../../common/logger')('auth');

const db = require('../../../models');

var errorResp = {
  error: {
    type: '',
    message: '',
  },
};

export class Controller {
  async login(req, res) {
    logger.info('starting login ...');
    if (validateResult(req, res) != true) {
      return;
    }

    try {
      const query = `select 
          u.*
      from 
          users u
      where 
          u.username = '${req.body.username}'
          AND u.password = '${req.body.password}'`;
      const user = await db.sequelize.query(query, {
        type: QueryTypes.SELECT,
        raw: true,
      });

      if (user === null || user[0] === undefined) {
        errorResp.error.type = 'process_invalid';
        errorResp.error.message = 'username or password incorrect';
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info('cannot login because username or password incorrect');
        return;
      }

      if (user[0].status === 'inactive') {
        errorResp.error.type = 'process_invalid';
        errorResp.error.message =
          'this user is inactive, please contact administer';
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info('cannot login because username or password incorrect');
        return;
      }

      const roleQuery = `select
          r.*
      from 
          user_roles ur, 
          roles r
      where 
          r.id = ur.role_id
          AND ur.user_id = '${user[0].id}'`;
      const role = await db.sequelize.query(roleQuery, {
        type: QueryTypes.SELECT,
        raw: true,
      });

      if (role !== null) {
        user[0].role = role[0];
      }

      const shopQuery = `select
          s.*
      from 
          user_shops us, 
          shops s
      where 
          s.id = us.shop_id
          AND us.user_id = '${user[0].id}'`;
      const shop = await db.sequelize.query(shopQuery, {
        type: QueryTypes.SELECT,
        raw: true,
      });

      if (shop !== null) {
        user[0].shop = shop[0];
      }

      const customResp = {
        code: 0,
        message: 'login successfully',
        data: user[0],
      };
      res.send(customResp);
      logger.info('login successfully');
    } catch (error) {
      logger.error(
        `code:${error.code} message:${error.message} stack:${error.stack}`
      );
      errorResp.error.type = 'process_invalid';
      errorResp.error.message = error.message;
      res.statusCode = StatusCodes.BAD_REQUEST;
      res.send(errorResp);
    }
    return;
  }
}

export default new Controller();
