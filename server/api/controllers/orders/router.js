import * as express from 'express';
import controller from './controller';
import validate from './validate';

export default express
  .Router()
  .get('/list', validate('list'), controller.listOrders)
  .get('/:id', validate('get'), controller.getOrder)
  .post('/create', controller.createOrder)
  .put('/:id', validate('update'), controller.updateOrder);
