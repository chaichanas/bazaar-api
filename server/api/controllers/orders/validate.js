import { checkSchema } from 'express-validator';

const listSchema = {
  page: {
    optional: true,
    isInt: {
      options: {
        min: 1,
      },
      errorMessage: {
        type: 'param_invalid',
        message: '`page` must be at least 1',
      },
    },
  },
  limit: {
    optional: true,
    isInt: {
      options: {
        min: 1,
        // max: 20,
      },
      errorMessage: {
        type: 'param_invalid',
        // message: '`limit` must be at least 1 and must not exceed 20',
        message: '`limit` must be at least 1',
      },
    },
  },
};

const getSchema = {
  id: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: 'order `id` cannot be empty',
      },
    },
  },
};

const updateSchema = {
  id: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: 'order `id` cannot be empty',
      },
    },
  },
  status: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`status` cannot be empty',
      },
    },
    isIn: {
      options: [['preparing', 'shipping', 'shipped', 'success', 'fail']],
      errorMessage: {
        type: 'param_invalid',
        message: `status must be one of  ['preparing', 'shipping', 'shipped', 'success', 'fail']`,
      },
    },
  },
};

const validate = (method) => {
  switch (method) {
    case 'list': {
      return [checkSchema(listSchema)];
    }
    case 'get': {
      return [checkSchema(getSchema)];
    }
    case 'update': {
      return [checkSchema(updateSchema)];
    }
  }
};

export default validate;
