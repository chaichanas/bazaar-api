import { StatusCodes } from 'http-status-codes';
import validateResult from '../../utils/validateResult';
const { QueryTypes } = require('sequelize');
const logger = require('../../../common/logger')('order');

const db = require('../../../models');
const Order = db.OrderModel;
const Transaction = db.TransactionModel;
const User = db.UserModel;

var errorResp = {
  error: {
    type: '',
    message: '',
  },
};

export class Controller {
  async createOrder(req, res) {
    logger.info('starting create order ...');

    try {
      const price = await totalPrice(req.body.transaction);
      let isPaid = false;
      if (req.body.is_paid === 'true') {
        isPaid = true;
      }

      const user = await User.findOne({
        where: { username: req.body.username },
        raw: true,
      });

      if (user === null) {
        errorResp.error.type = 'process_invalid';
        errorResp.error.message = 'user not found';
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info('cannot create order because user not found');
        return;
      }

      await db.sequelize.transaction(async (t) => {
        logger.info('starting create order ...');
        const orderCreated = await Order.create(
          {
            user_id: user.id,
            status: 'preparing',
            is_paid: isPaid,
            price: price,
            created_at: Date.now(),
            updated_at: Date.now(),
          },
          { transaction: t }
        );

        logger.info('starting create transaction order info ...');
        const transactions = req.body.transaction;
        for (const trx in transactions) {
          await Transaction.create(
            {
              order_id: orderCreated.id,
              product_id: transactions[trx].product_id,
              price: transactions[trx].price,
              amount: transactions[trx].amount,
              created_at: Date.now(),
              updated_at: Date.now(),
            },
            { transaction: t }
          );
        }

        const customResp = {
          code: 0,
          message: `created order ${orderCreated.id} successfully`,
        };
        res.send(customResp);
        logger.info(`created order ${orderCreated.id} successfully`);
      });
    } catch (error) {
      logger.error(
        `code:${error.code} message:${error.message} stack:${error.stack}`
      );
      errorResp.error.type = 'process_invalid';
      errorResp.error.message = error.message;
      res.statusCode = StatusCodes.BAD_REQUEST;
      res.send(errorResp);
    }
    return;
  }

  async listOrders(req, res) {
    logger.info('starting get order list result ...');
    if (validateResult(req, res) != true) {
      return;
    }

    let page = 1;
    let limit = 10;
    if (req.query.page !== null && req.query.page !== undefined) {
      page = req.query.page;
    }
    if (req.query.limit !== null && req.query.limit !== undefined) {
      limit = req.query.limit;
    }

    let cause = '';

    if (
      req.query.search !== null &&
      req.query.search !== undefined &&
      req.query.search !== ''
    ) {
      cause += `AND CONCAT(
        o.user_id, ' ',
        lower(u.username), ' ',
        lower(u.firstname), ' ',
        lower(u.lastname), ' ',
        o.created_at::text, ' ') LIKE '%${req.query.search.toLowerCase()}%'`;
    }

    const query = `select 
        o.*, 
        u.username as username,
        CONCAT(u.firstname, ' ', u.lastname) as user_name
    from 
        orders o, 
        users u
    where 
        o.user_id = u.id
        ${cause}
    order by o.updated_at desc`;

    const rows = await db.sequelize.query(
      `${query} offset ${(page - 1) * limit} limit ${limit}`,
      {
        type: QueryTypes.SELECT,
        raw: true,
      }
    );
    const count = await db.sequelize.query(
      `SELECT COUNT(*) as rows FROM (${query}) Q`,
      { type: QueryTypes.SELECT, raw: true }
    );

    if (count !== 0) {
      for (const r in rows) {
        const trxQuery = `select 
            t.*, 
            p.name as product_name,
            p.description as product_description,
            p.price as product_price,
            p.image_url as product_image_url,
            s.name as shop_name,
            s.description as shop_description,
            s.image_url as shop_image_url
        from 
            transactions t, 
            products p,
            shops s,
            shop_products sp
        where 
            t.product_id = p.id
            AND p.id = sp.product_id
            AND s.id = sp.shop_id
            AND t.order_id = ${rows[r].id}
        order by t.updated_at desc`;

        const trxData = await db.sequelize.query(trxQuery, {
          type: QueryTypes.SELECT,
          raw: true,
        });

        rows[r].product = {
          count: trxData.length,
          data: trxData,
        };
      }
    }

    if (req.query.search_shop) {
      let newRow = [];
      rows.forEach((row) => {
        if (row.product.data[0].shop_name === req.query.search_shop) {
          newRow.push(row);
        }
      });
      const custResp = {
        count: newRow.length,
        data: newRow,
      };
      res.send(custResp);
      logger.info('getting order list successfully');
      return;
    }

    const customResp = {
      count: parseInt(count[0].rows),
      data: rows,
    };

    res.send(customResp);
    logger.info('getting order list successfully');
    return;
  }

  async getOrder(req, res) {
    logger.info('starting get order ...');
    if (validateResult(req, res) != true) {
      return;
    }

    try {
      const query = `select 
          o.*, 
          u.username as username,
          CONCAT(u.firstname, ' ', u.lastname) as user_name
      from 
          orders o, 
          users u
      where 
          o.user_id = u.id
          AND o.id = '${req.params.id}'`;
      const order = await db.sequelize.query(query, {
        type: QueryTypes.SELECT,
        raw: true,
      });

      if (order === null || order[0] === undefined) {
        errorResp.error.type = 'process_invalid';
        errorResp.error.message = 'order not found';
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info('cannot get order because order not found');
        return;
      }

      const trxQuery = `select 
            t.*, 
            p.name as product_name,
            p.description as product_description,
            p.price as product_price,
            p.image_url as product_image_url,
            s.id as shop_id,
            s.name as shop_name,
            s.description as shop_description,
            s.image_url as shop_image_url
        from 
            transactions t, 
            products p,
            shops s,
            shop_products sp
        where 
            t.product_id = p.id
            AND p.id = sp.product_id
            AND s.id = sp.shop_id
            AND t.order_id = ${req.params.id}
        order by t.updated_at desc`;

      const trxData = await db.sequelize.query(trxQuery, {
        type: QueryTypes.SELECT,
        raw: true,
      });

      order[0].product = {
        count: trxData.length,
        data: trxData,
      };

      res.send(order);
      logger.info('getting order successfully');
    } catch (error) {
      logger.error(
        `code:${error.code} message:${error.message} stack:${error.stack}`
      );
      errorResp.error.type = 'process_invalid';
      errorResp.error.message = error.message;
      res.statusCode = StatusCodes.BAD_REQUEST;
      res.send(errorResp);
    }
    return;
  }

  async updateOrder(req, res) {
    logger.info('starting update order ...');
    if (validateResult(req, res) != true) {
      return;
    }

    try {
      let status = req.body.status;
      const order = await Order.findOne({
        where: { id: req.params.id },
        raw: true,
      });

      if (order === null) {
        errorResp.error.type = 'process_invalid';
        errorResp.error.message = 'order not found';
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info('cannot update order because order not found');
        return;
      }

      switch (order.status) {
        case 'preparing':
          if (['preparing'].includes(status)) {
            errorResp.error.type = 'process_invalid';
            errorResp.error.message = `cannot update status from ${order.status} to ${status}`;
            res.statusCode = StatusCodes.BAD_REQUEST;
            res.send(errorResp);
            logger.info(
              `cannot update status from ${order.status} to ${status}`
            );
            return;
          }
          break;
        case 'shipping':
          if (['preparing', 'shipping'].includes(status)) {
            errorResp.error.type = 'process_invalid';
            errorResp.error.message = `cannot update status from ${order.status} to ${status}`;
            res.statusCode = StatusCodes.BAD_REQUEST;
            res.send(errorResp);
            logger.info(
              `cannot update status from ${order.status} to ${status}`
            );
            return;
          }
          break;
        case 'shipped':
          if (['preparing', 'shipping', 'shipped'].includes(status)) {
            errorResp.error.type = 'process_invalid';
            errorResp.error.message = `cannot update status from ${order.status} to ${status}`;
            res.statusCode = StatusCodes.BAD_REQUEST;
            res.send(errorResp);
            logger.info(
              `cannot update status from ${order.status} to ${status}`
            );
            return;
          }
          break;
        case 'success':
          if (
            ['preparing', 'shipping', 'shipped', 'success', 'fail'].includes(
              status
            )
          ) {
            errorResp.error.type = 'process_invalid';
            errorResp.error.message = `cannot update status from ${order.status} to ${status}`;
            res.statusCode = StatusCodes.BAD_REQUEST;
            res.send(errorResp);
            logger.info(
              `cannot update status from ${order.status} to ${status}`
            );
            return;
          }
          break;
        case 'fail':
          if (
            ['preparing', 'shipping', 'shipped', 'success', 'fail'].includes(
              status
            )
          ) {
            errorResp.error.type = 'process_invalid';
            errorResp.error.message = `cannot update status from ${order.status} to ${status}`;
            res.statusCode = StatusCodes.BAD_REQUEST;
            res.send(errorResp);
            logger.info(
              `cannot update status from ${order.status} to ${status}`
            );
            return;
          }
          break;
      }

      await Order.update(
        {
          status: req.body.status,
          updated_at: Date.now(),
        },
        {
          where: { id: req.params.id },
        }
      );

      const customResp = {
        code: 0,
        message: 'updated order successfully',
      };

      res.send(customResp);
      logger.info('updated order successfully');
    } catch (error) {
      logger.error(
        `code:${error.code} message:${error.message} stack:${error.stack}`
      );
      errorResp.error.type = 'process_invalid';
      errorResp.error.message = error.message;
      res.statusCode = StatusCodes.BAD_REQUEST;
      res.send(errorResp);
    }
  }
}

export default new Controller();

const totalPrice = async (transactions) => {
  let total = 0;
  for (const t in transactions) {
    total += transactions[t].price;
  }
  return total;
};
