import { StatusCodes } from 'http-status-codes';
import validateResult from '../../utils/validateResult';
const { QueryTypes } = require('sequelize');
const logger = require('../../../common/logger')('product');

const db = require('../../../models');
const Product = db.ProductModel;
const Shop = db.ShopModel;
const ShopProduct = db.ShopProductModel;

var errorResp = {
  error: {
    type: '',
    message: '',
  },
};

export class Controller {
  async getProduct(req, res) {
    logger.info('starting get product ...');
    if (validateResult(req, res) != true) {
      return;
    }

    try {
      const query = `select 
          p.*, 
          s.name as shop_name
      from 
          products p, 
          shop_products sp, 
          shops s
      where 
          p.id = sp.product_id
          AND s.id = sp.shop_id
          AND p.id = '${req.params.id}'`;
      const product = await db.sequelize.query(query, {
        type: QueryTypes.SELECT,
        raw: true,
      });

      if (product === null || product[0] === undefined) {
        errorResp.error.type = 'process_invalid';
        errorResp.error.message = 'product not found';
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info('cannot get product because product not found');
        return;
      }

      res.send(product);
      logger.info('getting product successfully');
    } catch (error) {
      logger.error(
        `code:${error.code} message:${error.message} stack:${error.stack}`
      );
      errorResp.error.type = 'process_invalid';
      errorResp.error.message = error.message;
      res.statusCode = StatusCodes.BAD_REQUEST;
      res.send(errorResp);
    }
    return;
  }

  async createProduct(req, res) {
    logger.info('starting create product ...');
    if (validateResult(req, res) != true) {
      return;
    }

    try {
      const checkShop = await Shop.findOne({
        where: { name: req.body.shop },
        raw: true,
      });

      if (checkShop === null) {
        errorResp.error.type = 'process_invalid';
        errorResp.error.message = 'shop not found';
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info('cannot create product because shop not found');
        return;
      }

      // First, we start a transaction and save it into a variable
      await db.sequelize.transaction(async (t) => {
        logger.info('starting create new product ...');
        const productCreated = await Product.create(
          {
            name: req.body.name,
            description: req.body.description,
            price: req.body.price,
            is_soldout: false,
            image_url: req.body.image_url,
            created_at: Date.now(),
            updated_at: Date.now(),
          },
          { transaction: t }
        );

        logger.info('starting create shop product info ...');
        await ShopProduct.create(
          {
            shop_id: checkShop.id,
            product_id: productCreated.id,
            created_at: Date.now(),
            updated_at: Date.now(),
          },
          { transaction: t }
        );

        const customResp = {
          code: 0,
          message: `created ${productCreated.name} successfully`,
        };
        res.send(customResp);
        logger.info(`created ${productCreated.name} successfully`);
      });
    } catch (error) {
      logger.error(
        `code:${error.code} message:${error.message} stack:${error.stack}`
      );
      errorResp.error.type = 'process_invalid';
      errorResp.error.message = error.message;
      res.statusCode = StatusCodes.BAD_REQUEST;
      res.send(errorResp);
    }
    return;
  }

  async updateProduct(req, res) {
    logger.info('starting update product ...');
    if (validateResult(req, res) != true) {
      return;
    }

    try {
      let isSoldout = false;
      if (req.body.is_soldout === 'true') {
        isSoldout = true;
      }

      const product = await Product.findOne({
        where: { id: req.params.id },
        raw: true,
      });

      if (product === null) {
        errorResp.error.type = 'process_invalid';
        errorResp.error.message = 'product not found';
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info('cannot update product because product not found');
        return;
      }

      await Product.update(
        {
          name: req.body.name,
          description: req.body.description,
          price: req.body.price,
          is_soldout: isSoldout,
          image_url: req.body.image_url,
          updated_at: Date.now(),
        },
        {
          where: { id: req.params.id },
        }
      );

      const customResp = {
        code: 0,
        message: 'updated product successfully',
      };

      res.send(customResp);
      logger.info('updated product successfully');
    } catch (error) {
      logger.error(
        `code:${error.code} message:${error.message} stack:${error.stack}`
      );
      errorResp.error.type = 'process_invalid';
      errorResp.error.message = error.message;
      res.statusCode = StatusCodes.BAD_REQUEST;
      res.send(errorResp);
    }
  }

  async deleteProduct(req, res) {
    logger.info('starting delete product ...');
    if (validateResult(req, res) != true) {
      return;
    }
    try {
      const product = await Product.findOne({
        where: { id: req.params.id },
        raw: true,
      });

      if (product === null) {
        errorResp.error.type = 'process_invalid';
        errorResp.error.message = 'product not found';
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info('cannot delete product because product not found');
        return;
      }

      const shopProduct = await ShopProduct.findOne({
        where: { product_id: req.params.id },
        raw: true,
      });

      if (shopProduct === null) {
        errorResp.error.type = 'process_invalid';
        errorResp.error.message = 'shop_product not found';
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info('cannot delete product because shop_product not found');
        return;
      }

      await Product.update(
        {
          is_soldout: true,
          updated_at: Date.now(),
        },
        {
          where: { id: req.params.id },
        }
      );

      const customResp = {
        code: 0,
        message: 'deleted product successfully',
      };

      res.send(customResp);
      logger.info('deleted product successfully');
    } catch (error) {
      logger.error(
        `code:${error.code} message:${error.message} stack:${error.stack}`
      );
      errorResp.error.type = 'process_invalid';
      errorResp.error.message = error.message;
      res.statusCode = StatusCodes.BAD_REQUEST;
      res.send(errorResp);
    }
  }
}

export default new Controller();
