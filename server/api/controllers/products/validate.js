import { checkSchema } from 'express-validator';

const getShopSchema = {
  id: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: 'product `id` cannot be empty',
      },
    },
  },
};

const createSchema = {
  name: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`name` cannot be empty',
      },
    },
  },
  price: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`price` cannot be empty',
      },
    },
    isFloat: {
      options: {
        min: 0.5,
      },
      errorMessage: {
        type: 'param_invalid',
        message: '`price` must be at least 0.5 (50 satang)',
      },
    },
  },
};

const updateSchema = {
  id: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: 'product `id` cannot be empty',
      },
    },
  },
  name: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`name` cannot be empty',
      },
    },
  },
  price: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`price` cannot be empty',
      },
    },
    isFloat: {
      options: {
        min: 0.5,
      },
      errorMessage: {
        type: 'param_invalid',
        message: '`price` must be at least 0.5 (50 satang)',
      },
    },
  },
  is_soldout: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`is_soldout` cannot be empty',
      },
    },
    isIn: {
      options: [['true', 'false']],
      errorMessage: {
        type: 'param_invalid',
        message: `is_soldout must be one of ['true', 'false']`,
      },
    },
  },
};

const validate = (method) => {
  switch (method) {
    case 'get': {
      return [checkSchema(getShopSchema)];
    }
    case 'create': {
      return [checkSchema(createSchema)];
    }
    case 'update': {
      return [checkSchema(updateSchema)];
    }
  }
};

export default validate;
