import * as express from 'express';
import controller from './controller';
import validate from './validate';

export default express
  .Router()
  .get('/:id', validate('get'), controller.getProduct)
  .post('/create', validate('create'), controller.createProduct)
  .put('/:id', validate('update'), controller.updateProduct)
  .delete('/:id', validate('get'), controller.deleteProduct);
