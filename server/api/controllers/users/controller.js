import { StatusCodes } from 'http-status-codes';
import validateResult from '../../utils/validateResult';
const { QueryTypes } = require('sequelize');
const logger = require('../../../common/logger')('user');

const db = require('../../../models');
const Role = db.RoleModel;
const User = db.UserModel;
const UserRole = db.UserRoleModel;

var errorResp = {
  error: {
    type: '',
    message: '',
  },
};

export class Controller {
  async registUser(req, res) {
    logger.info('starting register user ...');
    if (validateResult(req, res) != true) {
      return;
    }

    try {
      if (req.body.password !== req.body.re_password) {
        errorResp.error.type = 'process_invalid';
        errorResp.error.message = `password and re_password not matched`;
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info(
          `cannot register because password and re_password not matched`
        );
        return;
      }

      // check user already in database
      const checkUser = await User.findOne({
        where: { username: req.body.username },
        raw: true,
      });

      if (checkUser !== null) {
        errorResp.error.type = 'record_already_exists';
        errorResp.error.message = 'this username already exists';
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info('cannot register because this username already exists');
        return;
      }

      const checkRole = await Role.findOne({
        where: { name: req.body.role },
        raw: true,
      });

      if (checkRole === null) {
        errorResp.error.type = 'process_invalid';
        errorResp.error.message = `role ${req.body.role} not found`;
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info(`cannot register because role ${req.body.role} not found`);
        return;
      }

      // First, we start a transaction and save it into a variable
      await db.sequelize.transaction(async (t) => {
        logger.info('starting create new user ...');
        const user = await User.create(
          {
            username: req.body.username,
            password: req.body.password,
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email,
            phone_number: req.body.phone_number,
            address: req.body.address,
            date_of_birth: req.body.date_of_birth,
            image_url: req.body.image_url,
            status: 'active',
            created_at: Date.now(),
            updated_at: Date.now(),
          },
          { transaction: t }
        );

        logger.info('starting create user role info ...');
        await UserRole.create(
          {
            user_id: user.id,
            role_id: checkRole.id,
            created_at: Date.now(),
            updated_at: Date.now(),
          },
          { transaction: t }
        );
      });
      const userData = await getUser(req.body.username, req.body.password);

      const customResp = {
        code: 0,
        message: `created ${req.body.username} successfully`,
        data: userData,
      };
      res.send(customResp);
      logger.info(`created ${req.body.username} successfully`);
    } catch (error) {
      logger.error(
        `code:${error.code} message:${error.message} stack:${error.stack}`
      );
      errorResp.error.type = 'process_invalid';
      errorResp.error.message = error.message;
      res.statusCode = StatusCodes.BAD_REQUEST;
      res.send(errorResp);
    }
    return;
  }

  async updateUser(req, res) {
    logger.info('starting update user ...');
    if (validateResult(req, res) != true) {
      return;
    }

    try {
      let newPassword = req.body.password;
      if (
        (req.body.new_password !== null &&
          req.body.new_password !== undefined &&
          req.body.new_password !== '') ||
        (req.body.re_password !== null &&
          req.body.re_password !== undefined &&
          req.body.re_password !== '')
      ) {
        if (req.body.new_password !== req.body.re_password) {
          errorResp.error.type = 'process_invalid';
          errorResp.error.message = `new_password and re_password not matched`;
          res.statusCode = StatusCodes.BAD_REQUEST;
          res.send(errorResp);
          logger.info(
            `cannot update user because new_password and re_password not matched`
          );
          return;
        }
        newPassword = req.body.re_password;
      }

      const user = await User.findOne({
        where: { id: req.params.id },
        raw: true,
      });

      if (user === null) {
        errorResp.error.type = 'process_invalid';
        errorResp.error.message = 'user not found';
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info('cannot update user because user not found');
        return;
      }

      const role = await Role.findOne({
        where: { name: req.body.role },
        raw: true,
      });

      if (role === null) {
        errorResp.error.type = 'process_invalid';
        errorResp.error.message = `role ${req.body.role} not found`;
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info(`cannot register because role ${req.body.role} not found`);
        return;
      }

      await db.sequelize.transaction(async (t) => {
        await User.update(
          {
            password: newPassword,
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email,
            phone_number: req.body.phone_number,
            address: req.body.address,
            date_of_birth: req.body.date_of_birth,
            image_url: req.body.image_url,
            status: req.body.status,
            updated_at: Date.now(),
          },
          { where: { id: req.params.id }, transaction: t }
        );
        await UserRole.update(
          {
            role_id: role.id,
            updated_at: Date.now(),
          },
          { where: { user_id: req.params.id }, transaction: t }
        );
      });

      const userData = await getUser(user.username, newPassword);

      const customResp = {
        code: 0,
        message: 'updated user successfully',
        data: userData,
      };

      res.send(customResp);
      logger.info('updated user successfully');
    } catch (error) {
      logger.error(
        `code:${error.code} message:${error.message} stack:${error.stack}`
      );
      errorResp.error.type = 'process_invalid';
      errorResp.error.message = error.message;
      res.statusCode = StatusCodes.BAD_REQUEST;
      res.send(errorResp);
    }
  }
}

export default new Controller();

const getUser = async (username, password) => {
  const query = `select 
          u.*
      from 
          users u
      where 
          u.username = '${username}'
          AND u.password = '${password}'`;
  const user = await db.sequelize.query(query, {
    type: QueryTypes.SELECT,
    raw: true,
  });

  const roleQuery = `select
          r.*
      from 
          user_roles ur, 
          roles r
      where 
          r.id = ur.role_id
          AND ur.user_id = '${user[0].id}'`;
  const role = await db.sequelize.query(roleQuery, {
    type: QueryTypes.SELECT,
    raw: true,
  });

  if (role !== null) {
    user[0].role = role[0];
  }

  const shopQuery = `select
          s.*
      from 
          user_shops us, 
          shops s
      where 
          s.id = us.shop_id
          AND us.user_id = '${user[0].id}'`;
  const shop = await db.sequelize.query(shopQuery, {
    type: QueryTypes.SELECT,
    raw: true,
  });

  if (shop !== null) {
    user[0].shop = shop[0];
  }

  return user[0];
};
