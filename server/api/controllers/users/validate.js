import { checkSchema } from 'express-validator';

const registSchema = {
  username: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`username` cannot be empty',
      },
    },
    matches: {
      options: /^[a-zA-Z0-9._]*$/,
      errorMessage: {
        type: 'param_invalid',
        message: '`username` is invalid',
      },
    },
  },
  password: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`password` cannot be empty',
      },
    },
    matches: {
      options: /^[a-zA-Z0-9._]*$/,
      errorMessage: {
        type: 'param_invalid',
        message: '`password` is invalid',
      },
    },
  },
  re_password: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`re_password` cannot be empty',
      },
    },
    matches: {
      options: /^[a-zA-Z0-9._]*$/,
      errorMessage: {
        type: 'param_invalid',
        message: '`re_password` is invalid',
      },
    },
  },
  firstname: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`firstname` cannot be empty',
      },
    },
  },
  lastname: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`lastname` cannot be empty',
      },
    },
  },
  date_of_birth: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`date_of_birth` cannot be empty',
      },
    },
    matches: {
      options: /^\d{4}\/(0[1-9]|1[012])\/(0[1-9]|[12][0-9]|3[01])$/i,
      errorMessage: {
        type: 'process_invalid',
        message: 'date of birth is format YYYY/MM/DD',
      },
    },
  },
  phone_number: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`phone_number` cannot be empty',
      },
    },
    matches: {
      options: /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/i,
      errorMessage: {
        type: 'process_invalid',
        message: 'phone number is invalid',
      },
    },
  },
  role: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`role` cannot be empty',
      },
    },
  },
  email: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`email` cannot be empty',
      },
    },
    isEmail: {
      errorMessage: {
        type: 'param_invalid',
        message: '`email` is invalid',
      },
    },
  },
};

const updateUserSchema = {
  id: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: 'user `id` cannot be empty',
      },
    },
  },
  password: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`password` cannot be empty',
      },
    },
    matches: {
      options: /^[a-zA-Z0-9._]*$/,
      errorMessage: {
        type: 'param_invalid',
        message: '`password` is invalid',
      },
    },
  },
  new_password: {
    matches: {
      options: /^[a-zA-Z0-9._]*$/,
      errorMessage: {
        type: 'param_invalid',
        message: '`new_password` is invalid',
      },
    },
  },
  re_password: {
    matches: {
      options: /^[a-zA-Z0-9._]*$/,
      errorMessage: {
        type: 'param_invalid',
        message: '`re_password` is invalid',
      },
    },
  },
  firstname: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`firstname` cannot be empty',
      },
    },
  },
  lastname: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`lastname` cannot be empty',
      },
    },
  },
  date_of_birth: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`date_of_birth` cannot be empty',
      },
    },
    matches: {
      options: /^\d{4}\/(0[1-9]|1[012])\/(0[1-9]|[12][0-9]|3[01])$/i,
      errorMessage: {
        type: 'process_invalid',
        message: 'date of birth is format YYYY/MM/DD',
      },
    },
  },
  phone_number: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`phone_number` cannot be empty',
      },
    },
    matches: {
      options: /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/i,
      errorMessage: {
        type: 'process_invalid',
        message: 'phone number is invalid',
      },
    },
  },
  role: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`role` cannot be empty',
      },
    },
  },
  email: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`email` cannot be empty',
      },
    },
    isEmail: {
      errorMessage: {
        type: 'param_invalid',
        message: '`email` is invalid',
      },
    },
  },
};

const validate = (method) => {
  switch (method) {
    case 'regist': {
      return [checkSchema(registSchema)];
    }
    case 'update': {
      return [checkSchema(updateUserSchema)];
    }
  }
};

export default validate;
