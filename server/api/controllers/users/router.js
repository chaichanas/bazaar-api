import * as express from 'express';
import controller from './controller';
import validate from './validate';

export default express
  .Router()
  .post('/register', validate('regist'), controller.registUser)
  .put('/:id', validate('update'), controller.updateUser);
