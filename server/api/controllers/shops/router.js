import * as express from 'express';
import controller from './controller';
import validate from './validate';

export default express
  .Router()
  .get('/list', validate('list'), controller.listShops)
  .get('/:id', validate('get'), controller.getShop)
  .post('/create', validate('create'), controller.createShop)
  .put('/:id', validate('update'), controller.updateShop);
