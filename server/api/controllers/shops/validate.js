import { checkSchema } from 'express-validator';

const listSchema = {
  page: {
    optional: true,
    isInt: {
      options: {
        min: 1,
      },
      errorMessage: {
        type: 'param_invalid',
        message: '`page` must be at least 1',
      },
    },
  },
  limit: {
    optional: true,
    isInt: {
      options: {
        min: 1,
        // max: 20,
      },
      errorMessage: {
        type: 'param_invalid',
        // message: '`limit` must be at least 1 and must not exceed 20',
        message: '`limit` must be at least 1',
      },
    },
  },
};

const getShopSchema = {
  id: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: 'shop `id` cannot be empty',
      },
    },
  },
};

const createSchema = {
  username: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`username` cannot be empty',
      },
    },
    matches: {
      options: /^[a-zA-Z0-9._]*$/,
      errorMessage: {
        type: 'param_invalid',
        message: '`username` is invalid',
      },
    },
  },
  name: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`name` cannot be empty',
      },
    },
  },
  phone_number: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`phone_number` cannot be empty',
      },
    },
    matches: {
      options: /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/i,
      errorMessage: {
        type: 'process_invalid',
        message: 'phone number is invalid',
      },
    },
  },
  email: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`email` cannot be empty',
      },
    },
    isEmail: {
      errorMessage: {
        type: 'param_invalid',
        message: '`email` is invalid',
      },
    },
  },
};

const updateSchema = {
  id: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: 'shop `id` cannot be empty',
      },
    },
  },
  name: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`name` cannot be empty',
      },
    },
  },
  phone_number: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`phone_number` cannot be empty',
      },
    },
    matches: {
      options: /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/i,
      errorMessage: {
        type: 'process_invalid',
        message: 'phone number is invalid',
      },
    },
  },
  is_open: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`is_open` cannot be empty',
      },
    },
    isIn: {
      options: [['true', 'false']],
      errorMessage: {
        type: 'param_invalid',
        message: `is_open must be one of ['true', 'false']`,
      },
    },
  },
  email: {
    notEmpty: {
      errorMessage: {
        type: 'param_invalid',
        message: '`email` cannot be empty',
      },
    },
    isEmail: {
      errorMessage: {
        type: 'param_invalid',
        message: '`email` is invalid',
      },
    },
  },
};

const validate = (method) => {
  switch (method) {
    case 'list': {
      return [checkSchema(listSchema)];
    }
    case 'get': {
      return [checkSchema(getShopSchema)];
    }
    case 'create': {
      return [checkSchema(createSchema)];
    }
    case 'update': {
      return [checkSchema(updateSchema)];
    }
  }
};

export default validate;
