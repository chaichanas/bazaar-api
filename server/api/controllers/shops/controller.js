import { StatusCodes } from 'http-status-codes';
import validateResult from '../../utils/validateResult';
const { QueryTypes } = require('sequelize');
const logger = require('../../../common/logger')('shop');

const db = require('../../../models');
const Shop = db.ShopModel;
const UserShop = db.UserShopModel;

var errorResp = {
  error: {
    type: '',
    message: '',
  },
};

export class Controller {
  async listShops(req, res) {
    logger.info('starting get shop list result ...');
    if (validateResult(req, res) != true) {
      return;
    }

    let page = 1;
    let limit = 10;
    if (req.query.page !== null && req.query.page !== undefined) {
      page = req.query.page;
    }
    if (req.query.limit !== null && req.query.limit !== undefined) {
      limit = req.query.limit;
    }

    let cause = '';
    if (
      req.query.search !== null &&
      req.query.search !== undefined &&
      req.query.search !== ''
    ) {
      cause += `AND lower(s.name) LIKE '%${req.query.search.toLowerCase()}%'`;
    }

    const query = `select 
        s.*, 
        u.username as username
    from 
        shops s, 
        user_shops us, 
        users u
    where 
        s.id = us.shop_id
        AND u.id = us.user_id
        ${cause}
    order by s.updated_at desc`;

    const rows = await db.sequelize.query(
      `${query} offset ${(page - 1) * limit} limit ${limit}`,
      {
        type: QueryTypes.SELECT,
        raw: true,
      }
    );
    const count = await db.sequelize.query(
      `SELECT COUNT(*) as rows FROM (${query}) Q`,
      { type: QueryTypes.SELECT, raw: true }
    );
    const customResp = {
      count: parseInt(count[0].rows),
      data: rows,
    };

    res.send(customResp);
    logger.info('getting shop list successfully');
    return;
  }

  async getShop(req, res) {
    logger.info('starting get shop ...');
    if (validateResult(req, res) != true) {
      return;
    }

    try {
      const query = `select 
          s.*, 
          u.username as username
      from 
          shops s, 
          user_shops us, 
          users u
      where 
          s.id = us.shop_id
          AND u.id = us.user_id
          AND s.id = '${req.params.id}'`;
      const shop = await db.sequelize.query(query, {
        type: QueryTypes.SELECT,
        raw: true,
      });

      if (shop === null || shop[0] === undefined) {
        errorResp.error.type = 'process_invalid';
        errorResp.error.message = 'shop not found';
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info('cannot get shop because shop not found');
        return;
      }

      const productQuery = `select 
          p.*, 
          s.name as shop_name
      from 
          products p, 
          shop_products sp, 
          shops s
      where 
          p.id = sp.product_id
          AND p.is_soldout IS NOT TRUE
          AND s.id = sp.shop_id
          AND s.id = '${req.params.id}'
      order by p.updated_at desc`;
      const product = await db.sequelize.query(productQuery, {
        type: QueryTypes.SELECT,
        raw: true,
      });

      shop[0].product = {
        count: product.length,
        data: product,
      };

      res.send(shop);
      logger.info('getting shop successfully');
    } catch (error) {
      logger.error(
        `code:${error.code} message:${error.message} stack:${error.stack}`
      );
      errorResp.error.type = 'process_invalid';
      errorResp.error.message = error.message;
      res.statusCode = StatusCodes.BAD_REQUEST;
      res.send(errorResp);
    }
    return;
  }

  async createShop(req, res) {
    logger.info('starting create shop ...');
    if (validateResult(req, res) != true) {
      return;
    }

    try {
      const query = `select 
          u.*, 
          r.name as role
      from 
          users u, 
          user_roles ur, 
          roles r
      where 
          u.id = ur.user_id
          AND r.id = ur.role_id
          AND u.username = '${req.body.username}'`;
      const user = await db.sequelize.query(query, {
        type: QueryTypes.SELECT,
        raw: true,
      });

      if (user === null || user[0] === undefined) {
        errorResp.error.type = 'process_invalid';
        errorResp.error.message = 'user not found';
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info('cannot create shop because user not found');
        return;
      }

      if (user[0].role === 'member') {
        errorResp.error.type = 'process_invalid';
        errorResp.error.message = 'this role cannot create shop';
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info('cannot create shop because this role cannot create shop');
        return;
      }

      const checkUserShop = await UserShop.findOne({
        where: { user_id: user[0].id },
        raw: true,
      });

      if (checkUserShop !== null) {
        errorResp.error.type = 'record_already_exists';
        errorResp.error.message = 'this user already exists';
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info('cannot create shop because this user already exists');
        return;
      }

      const checkShop = await Shop.findOne({
        where: { name: req.body.name },
        raw: true,
      });

      if (checkShop !== null) {
        errorResp.error.type = 'record_already_exists';
        errorResp.error.message = 'this name already exists';
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info('cannot create shop because this name already exists');
        return;
      }

      // First, we start a transaction and save it into a variable
      await db.sequelize.transaction(async (t) => {
        logger.info('starting create new shop ...');
        const shop = await Shop.create(
          {
            name: req.body.name,
            description: req.body.description,
            address: req.body.address,
            email: req.body.email,
            phone_number: req.body.phone_number,
            is_open: true,
            image_url: req.body.image_url,
            created_at: Date.now(),
            updated_at: Date.now(),
          },
          { transaction: t }
        );

        logger.info('starting create user shop info ...');
        await UserShop.create(
          {
            user_id: user[0].id,
            shop_id: shop.id,
            created_at: Date.now(),
            updated_at: Date.now(),
          },
          { transaction: t }
        );

        const customResp = {
          code: 0,
          message: `created ${shop.name} successfully`,
        };
        res.send(customResp);
        logger.info(`created ${shop.name} successfully`);
      });
    } catch (error) {
      logger.error(
        `code:${error.code} message:${error.message} stack:${error.stack}`
      );
      errorResp.error.type = 'process_invalid';
      errorResp.error.message = error.message;
      res.statusCode = StatusCodes.BAD_REQUEST;
      res.send(errorResp);
    }
    return;
  }

  async updateShop(req, res) {
    logger.info('starting update shop ...');
    if (validateResult(req, res) != true) {
      return;
    }

    try {
      let isOpen = false;
      if (req.body.is_open === 'true') {
        isOpen = true;
      }

      const shop = await Shop.findOne({
        where: { id: req.params.id },
        raw: true,
      });

      if (shop === null) {
        errorResp.error.type = 'process_invalid';
        errorResp.error.message = 'shop not found';
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info('cannot update shop because shop not found');
        return;
      }

      const checkShop = await Shop.findOne({
        where: { name: req.body.name },
        raw: true,
      });

      if (shop.id !== checkShop?.id && checkShop?.name === req.body.name) {
        errorResp.error.type = 'record_already_exists';
        errorResp.error.message = 'this name already exists';
        res.statusCode = StatusCodes.BAD_REQUEST;
        res.send(errorResp);
        logger.info('cannot update shop because this name already exists');
        return;
      }

      await Shop.update(
        {
          name: req.body.name,
          description: req.body.description,
          address: req.body.address,
          email: req.body.email,
          phone_number: req.body.phone_number,
          is_open: isOpen,
          image_url: req.body.image_url,
          updated_at: Date.now(),
        },
        {
          where: { id: req.params.id },
        }
      );

      const customResp = {
        code: 0,
        message: 'updated shop successfully',
      };

      res.send(customResp);
      logger.info('updated shop successfully');
    } catch (error) {
      logger.error(
        `code:${error.code} message:${error.message} stack:${error.stack}`
      );
      errorResp.error.type = 'process_invalid';
      errorResp.error.message = error.message;
      res.statusCode = StatusCodes.BAD_REQUEST;
      res.send(errorResp);
    }
  }
}

export default new Controller();
