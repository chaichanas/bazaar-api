module.exports = function (sequelize, Sequelize) {
  const UserRole = sequelize.define(
    'user_roles',
    {
      id: {
        type: Sequelize.BIGINT,
        autoIncrement: true,
        primaryKey: true,
      },
      user_id: {
        type: Sequelize.BIGINT,
        references: {
          model: 'users',
          key: 'id',
        },
        onDelete: 'cascade',
      },
      role_id: {
        type: Sequelize.BIGINT,
        references: {
          model: 'roles',
          key: 'id',
        },
        onDelete: 'cascade',
      },
      created_at: { type: Sequelize.DATE },
      updated_at: { type: Sequelize.DATE },
    },
    {
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    }
  );

  return UserRole;
};
