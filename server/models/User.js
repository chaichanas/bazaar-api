module.exports = function (sequelize, Sequelize) {
  const User = sequelize.define(
    'users',
    {
      id: {
        type: Sequelize.BIGINT,
        autoIncrement: true,
        primaryKey: true,
      },
      username: { type: Sequelize.STRING, unique: true },
      password: { type: Sequelize.TEXT },
      firstname: { type: Sequelize.STRING },
      lastname: { type: Sequelize.STRING },
      email: { type: Sequelize.STRING },
      phone_number: { type: Sequelize.STRING },
      address: { type: Sequelize.TEXT },
      date_of_birth: { type: Sequelize.DATE },
      image_url: { type: Sequelize.TEXT },
      status: {
        type: Sequelize.ENUM({
          name: 'enum_status_id',
          values: ['active', 'inactive'],
        }),
      },
      created_at: { type: Sequelize.DATE },
      updated_at: { type: Sequelize.DATE },
    },
    {
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    }
  );

  return User;
};
