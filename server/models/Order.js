module.exports = function (sequelize, Sequelize) {
  const Order = sequelize.define(
    'orders',
    {
      id: {
        type: Sequelize.BIGINT,
        autoIncrement: true,
        primaryKey: true,
      },
      user_id: {
        type: Sequelize.BIGINT,
        references: {
          model: 'users',
          key: 'id',
        },
        onDelete: 'cascade',
      },
      status: {
        type: Sequelize.ENUM({
          name: 'enum_status_id',
          values: ['preparing', 'shipping', 'shipped', 'success', 'fail'],
        }),
      },
      is_paid: { type: Sequelize.BOOLEAN },
      price: { type: Sequelize.DOUBLE },
      created_at: { type: Sequelize.DATE },
      updated_at: { type: Sequelize.DATE },
    },
    {
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    }
  );

  return Order;
};
