module.exports = function (sequelize, Sequelize) {
  const UserShop = sequelize.define(
    'user_shops',
    {
      id: {
        type: Sequelize.BIGINT,
        autoIncrement: true,
        primaryKey: true,
      },
      user_id: {
        type: Sequelize.BIGINT,
        references: {
          model: 'users',
          key: 'id',
        },
        onDelete: 'cascade',
      },
      shop_id: {
        type: Sequelize.BIGINT,
        references: {
          model: 'shops',
          key: 'id',
        },
        onDelete: 'cascade',
      },
      created_at: { type: Sequelize.DATE },
      updated_at: { type: Sequelize.DATE },
    },
    {
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    }
  );

  return UserShop;
};
