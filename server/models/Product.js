module.exports = function (sequelize, Sequelize) {
  const Product = sequelize.define(
    'products',
    {
      id: {
        type: Sequelize.BIGINT,
        autoIncrement: true,
        primaryKey: true,
      },
      name: { type: Sequelize.STRING },
      description: { type: Sequelize.TEXT },
      price: { type: Sequelize.DOUBLE },
      is_soldout: { type: Sequelize.BOOLEAN },
      image_url: { type: Sequelize.TEXT },
      created_at: { type: Sequelize.DATE },
      updated_at: { type: Sequelize.DATE },
    },
    {
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    }
  );

  return Product;
};
