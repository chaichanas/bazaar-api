module.exports = function (sequelize, Sequelize) {
  const Shop = sequelize.define(
    'shops',
    {
      id: {
        type: Sequelize.BIGINT,
        autoIncrement: true,
        primaryKey: true,
      },
      name: { type: Sequelize.STRING, unique: true },
      description: { type: Sequelize.TEXT },
      address: { type: Sequelize.TEXT },
      email: { type: Sequelize.STRING },
      phone_number: { type: Sequelize.STRING },
      is_open: { type: Sequelize.BOOLEAN },
      image_url: { type: Sequelize.TEXT },
      created_at: { type: Sequelize.DATE },
      updated_at: { type: Sequelize.DATE },
    },
    {
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    }
  );

  return Shop;
};
