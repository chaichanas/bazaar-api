import { Sequelize } from 'sequelize';

const DB_NAME = process.env.DB_NAME || process.env['DB_DATABASE'];
const DB_USER = process.env.DB_USER || process.env['DB_USERNAME'];
const DB_PASS = process.env.DB_PASS || process.env['DB_PASSWORD'];
const DB_HOST = process.env.DB_HOST || process.env['DB_HOSTNAME'];

const db = {};

const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASS, {
  host: DB_HOST,
  dialect: 'postgresql',
  logging: false,
  pool: {
    max: 5,
    idle: 10000,
    acquire: 60000,
  },
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.OrderModel = require('./Order.js')(sequelize, Sequelize);
db.ProductModel = require('./Product.js')(sequelize, Sequelize);
db.RoleModel = require('./Role.js')(sequelize, Sequelize);
db.ShopModel = require('./Shop.js')(sequelize, Sequelize);
db.ShopProductModel = require('./ShopProduct.js')(sequelize, Sequelize);
db.TransactionModel = require('./Transaction.js')(sequelize, Sequelize);
db.UserModel = require('./User.js')(sequelize, Sequelize);
db.UserRoleModel = require('./UserRole.js')(sequelize, Sequelize);
db.UserShopModel = require('./UserShop.js')(sequelize, Sequelize);

module.exports = db;
