module.exports = function (sequelize, Sequelize) {
  const ShopProduct = sequelize.define(
    'shop_products',
    {
      id: {
        type: Sequelize.BIGINT,
        autoIncrement: true,
        primaryKey: true,
      },
      shop_id: {
        type: Sequelize.BIGINT,
        references: {
          model: 'shops',
          key: 'id',
        },
        onDelete: 'cascade',
      },
      product_id: {
        type: Sequelize.BIGINT,
        references: {
          model: 'products',
          key: 'id',
        },
        onDelete: 'cascade',
      },
      created_at: { type: Sequelize.DATE },
      updated_at: { type: Sequelize.DATE },
    },
    {
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    }
  );

  return ShopProduct;
};
