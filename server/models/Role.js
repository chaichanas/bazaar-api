module.exports = function (sequelize, Sequelize) {
  const Role = sequelize.define(
    'roles',
    {
      id: {
        type: Sequelize.BIGINT,
        autoIncrement: true,
        primaryKey: true,
      },
      name: { type: Sequelize.STRING, unique: true },
      description: { type: Sequelize.TEXT },
      created_at: { type: Sequelize.DATE },
      updated_at: { type: Sequelize.DATE },
    },
    {
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    }
  );
  return Role;
};
