module.exports = function (sequelize, Sequelize) {
  const Transaction = sequelize.define(
    'transactions',
    {
      id: {
        type: Sequelize.BIGINT,
        autoIncrement: true,
        primaryKey: true,
      },
      order_id: {
        type: Sequelize.BIGINT,
        references: {
          model: 'orders',
          key: 'id',
        },
        onDelete: 'cascade',
      },
      product_id: {
        type: Sequelize.BIGINT,
        references: {
          model: 'products',
          key: 'id',
        },
        onDelete: 'cascade',
      },
      price: { type: Sequelize.DOUBLE },
      amount: { type: Sequelize.INTEGER },
      created_at: { type: Sequelize.DATE },
      updated_at: { type: Sequelize.DATE },
    },
    {
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
    }
  );

  return Transaction;
};
