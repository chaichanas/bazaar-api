import authRouter from './api/controllers/auth/router';
import orderRouter from './api/controllers/orders/router';
import productRouter from './api/controllers/products/router';
import shopRouter from './api/controllers/shops/router';
import userRouter from './api/controllers/users/router';

export default function routes(app) {
  app.use('/v1/auth', authRouter);
  app.use('/v1/orders', orderRouter);
  app.use('/v1/products', productRouter);
  app.use('/v1/shops', shopRouter);
  app.use('/v1/users', userRouter);
}
