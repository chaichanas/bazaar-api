const { validationResult } = require('express-validator');
import { StatusCodes } from 'http-status-codes';
const validateresult = (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res
      .status(StatusCodes.BAD_REQUEST)
      .json({ error: errors.array()[0].msg });
  } else {
    return true;
  }
};

module.exports = validateresult;
