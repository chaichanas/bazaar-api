import dotenv from 'dotenv';

if (process.env.ENV_FILE) {
  dotenv.config({ path: `.env.${process.env.ENV_FILE}` });
} else {
  dotenv.config();
}
