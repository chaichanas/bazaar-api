import pino from 'pino';

module.exports = function (name) {
  return pino({ name: name, level: process.env['LOG_LEVEL'] });
};
